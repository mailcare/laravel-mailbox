<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use BeyondCode\Mailbox\InboundEmail;
use BeyondCode\Mailbox\Facades\Mailbox;
use Illuminate\Support\Facades\Log;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        
        Mailbox::from('customers@wormly.com', function (InboundEmail $email) {
            
            Log::info('Email received by laravel-mailbox');
            Log::info($email->subject());
            Log::info($email->from());
            Log::info($email->to());
        });
    }
}
