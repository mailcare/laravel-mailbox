<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('email-received', function (Request $request){


    // With Post the raw, full MIME message checked
    Log::info('Email received by raw');
    Log::info($request->getContent());

    // Json format
    Log::info('Email received by JSON');
    Log::info($request->input('data.subject'));
    Log::info($request->input('data.sender.email'));
    Log::info($request->input('data.inbox.email'));


    // Json format
    Log::info('Email received by post');
    Log::info($request->post());
});
